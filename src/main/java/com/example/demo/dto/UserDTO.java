package com.example.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Email;
import org.springframework.format.annotation.DateTimeFormat;

import java.text.DateFormat;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
public class UserDTO {
    private String username;
    @Email
    private String  email;
    private String name;
    private String surname;
    @DateTimeFormat
    //private Date birthDate;
    private String cityname;
    private String password;
    private String passwordConfirm;
}
