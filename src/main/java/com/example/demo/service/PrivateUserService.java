package com.example.demo.service;

import com.example.demo.model.Classified;
import com.example.demo.model.PrivateUser;
import com.example.demo.repository.ClassifiedRepository;
import com.example.demo.repository.PrivateUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PrivateUserService {
    private final PrivateUserRepository privateUserRepository;

    @Autowired
    public PrivateUserService(PrivateUserRepository privateUserRepository) {
        this.privateUserRepository= privateUserRepository;
    }
//    public void addClassified(PrivateUser privateUser, Classified classified){
//        classifiedRepository.save(classified);
//    }
    public void addUser(PrivateUser privateUser){
        this.privateUserRepository.save(privateUser);
    }
    public List<PrivateUser> getAllUsers(){
        return privateUserRepository.findAll();
    }
    public PrivateUser findByUsername(String username){
        return  this.privateUserRepository.findFirstByUsername(username);
    }

}
