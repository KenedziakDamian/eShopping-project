package com.example.demo.service;

import com.example.demo.model.Classified;
import com.example.demo.model.PrivateUser;
import com.example.demo.repository.ClassifiedRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClassifiedService {
    private final ClassifiedRepository classifiedRepository;

    @Autowired
    public ClassifiedService(ClassifiedRepository classifiedRepository) {
        this.classifiedRepository  = classifiedRepository;
    }
    public void addClassified(PrivateUser privateUser, Classified classified){

        classifiedRepository.save(classified);
    }
    public List<Classified> getAllClassifieds(){
        return classifiedRepository.findAll();
    }

}
