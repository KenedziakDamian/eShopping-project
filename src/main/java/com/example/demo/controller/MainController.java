package com.example.demo.controller;

import com.example.demo.dto.UserDTO;
import com.example.demo.model.Classified;
import com.example.demo.model.PrivateUser;
import com.example.demo.service.ClassifiedService;
import com.example.demo.service.PrivateUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;
import java.util.PrimitiveIterator;
import java.util.Set;

@Controller
public class MainController {

    private final ClassifiedService classifiedService;
    private final PrivateUserService privateUserService;
    @Autowired
    public MainController(ClassifiedService classifiedService, PrivateUserService privateUserService) {

        this.classifiedService = classifiedService;
        this.privateUserService = privateUserService;
    }
    @RequestMapping("/")
    public ModelAndView getHome(){
        PrivateUser tmp = privateUserService.findByUsername("kenedziak");
        Classified classified = new Classified("Tytul");
        //tmp.addClassified(classified);
        //classifiedService.addClassified(classified);
        return new ModelAndView("index");
        }

    @RequestMapping("/addUser")
    public ModelAndView getClassifieds(){
        privateUserService.addUser(new PrivateUser(new UserDTO("kenedziak",
                "damian@o2.pl","Damian","Gabiec","Białystok","toto","toto")));
        //        ModelAndView modelAndView = new ModelAndView();
//        modelAndView.addObject(classifiedService.getAllClassifieds());
        return new ModelAndView("index");
    }



}


