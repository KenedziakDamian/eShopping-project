package com.example.demo.repository;


import com.example.demo.model.Classified;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
@Repository
public interface ClassifiedRepository extends JpaRepository<Classified,Long> {
    Collection<Classified> findByTitle(String name);
    Collection<Classified>  findById(Long id);

}

