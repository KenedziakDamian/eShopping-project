package com.example.demo.repository;

import com.example.demo.model.Classified;
import com.example.demo.model.PrivateUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface PrivateUserRepository extends JpaRepository<PrivateUser,Long> {
    PrivateUser findOneByName(String name);
    PrivateUser  findById(Long id);
    PrivateUser findOneByUsername(String username);
    PrivateUser findFirstByUsername(String username);
}