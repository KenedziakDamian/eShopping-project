package com.example.demo.model;

import com.example.demo.dto.UserDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Email;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.text.DateFormat;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static javax.persistence.CascadeType.ALL;

@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class PrivateUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true)
    private Long id;
    private String name;
    private String username;
    private String password;
    private String city;
    private DateFormat birthDate;
   // @Email
    private String email;
    @OneToMany
    private List<Classified> classifieds;


    public PrivateUser (UserDTO userDTO){
        name = userDTO.getName();
        username = userDTO.getUsername();
        password = userDTO.getPassword();
        city = userDTO.getCityname();
        email = userDTO.getEmail();
       // birthDate = userDTO.getBirthDate();
         }

    public List<Classified> getClassifieds() {
        return classifieds;
    }

    public void setClassifieds(List<Classified> classifieds) {
        this.classifieds = classifieds;
    }
}


